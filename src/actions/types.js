export const TOKEN_GET_START = 'token_get_start';
export const TOKEN_GET_SUCCESS = 'token_get_success';
export const TOKEN_GET_FAIL = 'token_get_fail';

export const USERID_GET_SUCCESS = 'userid_get_success';
export const USERID_GET_FAIL = 'userid_get_fail';
export const USERID_GET_START = 'userid_get_start';

export const ACTIVITIES_GET_START = 'activities_get_start';
export const ACTIVITIES_GET_SUCCESS = 'activity_get_success';
export const ACTIVITIES_GET_FAIL = 'activity_get_fail';

export const MY_ACTIVITIES_GET_START = 'my_activities_get_start';
export const MY_ACTIVITIES_GET_SUCCESS = 'my_activity_get_success';
export const MY_ACTIVITIES_GET_FAIL = 'my_activity_get_fail';

export const ACTIVITY_JOIN_START = 'activity_join_start';
export const ACTIVITY_JOIN_SUCCESS = 'activity_join_success';
export const ACTIVITY_JOIN_FAIL = 'activity_join_fail';

export const ACTIVITY_LEAVE_START = 'activity_leave_start';
export const ACTIVITY_LEAVE_SUCCESS = 'activity_leave_success';
export const ACTIVITY_LEAVE_FAIL = 'activity_leave_fail';

export const ACTIVITY_CREATE_START = 'activity_create_start';
export const ACTIVITY_CREATE_FAIL = 'activity_create_fail';
export const ACTIVITY_CREATE_SUCCESS = 'activity_create_success';
export const ACTIVITY_CREATE_UPDATE_FORM = 'activity_create_update_form';
export const ACTIVITY_CREATE_CLEAN_FORM = 'activity_create_clean_form';

export const ADDRESS_CHANGED = 'address_changed';
export const SPORT_CHANGED = 'sport_changed';
export const GPS_START = 'gps_start';
export const GPS_SUCCESS = 'gps_success';
export const GPS_ERROR = 'gps_error';

export const LOADING_START = 'loading_start';
export const LOADING_STOP = 'loading_stop';
export const SHOW_MESSAGE = 'show_message';

export const PUSH_ROUTE = 'PUSH_ROUTE';
export const POP_ROUTE = 'POP_ROUTE';

export const ADDRESS_VENUE_CHANGED = 'address_venue_changed';
export const VENUE_SELECTED = 'venue_selected';
export const VENUES_GET_START = 'venues_get_start';
export const VENUES_GET_SUCCESS = 'venues_get_success';
export const VENUES_GET_FAIL = 'venues_get_fail';

export const CLEAR_NOTIFICATION = 'clear_notification';

export const OFFLINE = 'offline';

export const GEOCODE_STARTED = 'GEOCODE_STARTED'
export const GEOCODE_SUCCESS = 'GEOCODE_SUCCESS'
export const GEOCODE_ERROR = 'GEOCODE_ERROR'
export const REVERSE_GEOCODING_SUCCESS = 'REVERSE_GEOCODING_SUCCESS'
export const REVERSE_GEOCODING_ERROR = 'REVERSE_GEOCODING_ERROR'
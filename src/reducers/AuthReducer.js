// TODO

import {
    TOKEN_GET_START,
    TOKEN_GET_SUCCESS,
    TOKEN_GET_FAIL,
    USERID_GET_SUCCESS,
    USERID_GET_START,
    USERID_GET_FAIL
} from '../actions/types';

const INITIAL_STATE = {
    token: null,
    userID: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TOKEN_GET_START:
            return state;
        case TOKEN_GET_SUCCESS:
            return { ...state, token: action.payload };
        case TOKEN_GET_FAIL:
            return state;
        case USERID_GET_START:
            return state;
        case USERID_GET_FAIL:
            return state;
        case USERID_GET_SUCCESS:
            return { ...state, userID: action.payload };
        default:
            return state;
    }
};

import {
    ADDRESS_CHANGED,
    SPORT_CHANGED,
    GEOCODE_STARTED,
    GEOCODE_SUCCESS,
    GEOCODE_ERROR,
    REVERSE_GEOCODING_SUCCESS,
    REVERSE_GEOCODING_ERROR
} from './types';

import { getCurrentLocation } from './LocationActions'

import Config from 'react-native-config';

export const addressChanged = (address) => {
    return {
        type: ADDRESS_CHANGED,
        payload: address
    };
};

export const sportChanged = (sport) => {
    return {
        type: SPORT_CHANGED,
        payload: sport
    }
};

export const googleMapsReverseGeocode = () => {
    return (dispatch, getState) => {
        const state = getState()
        const { gps } = state.location
        const { token } = state.auth

        if (!gps) {
            return dispatch(getCurrentLocation())
                .then(gpsData => {
                    return useGpsResults(gpsData, token, dispatch)
                })
                .catch(() => {
                    // if the GPS is disabled we cannot establish the current state.
                    // We leave it null and let the user type it manually if he needs
                    return Promise.resolve(null)
                })
        } else {
            return useGpsResults(gps, token, dispatch)
        }
    }
}

const useGpsResults = (gps, token, dispatch) => {
    const lat = gps.n
    const lng = gps.e

    return fetch(`${Config.API_URL}/reverseGeocode?lat=${lat}&lng=${lng}`, {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
    .then(response => {
        if (!response.ok) {
            const error = {
                code: response.status,
                message: response.statusText
            }
            dispatch({type: REVERSE_GEOCODING_ERROR, payload: error})
            throw error
        }
        return response.json()
    })
    .then(response => {
        return dispatch({
            type: REVERSE_GEOCODING_SUCCESS, payload: response.data
        })
    })
    .catch( error => {
        console.log(error)
        throw error
    })
}

export const googleMapsGeocode = (searchQuery) => {
    return (dispatch, getState) => {
        const state = getState()
        const { currentCountry, currentLocality } = state.search
        const { token } = state.auth

        let url = `${Config.API_URL}/geocode?search=${searchQuery}`

        if (currentCountry) {
            url += `&country=${currentCountry}`
        }

        if (currentLocality) {
            url += `&locality=${currentLocality}`
        }

        dispatch({type: GEOCODE_STARTED, payload: searchQuery})

        return fetch(url, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
        .then( response => {
            if (!response.ok) {
                const error = {
                    code: response.status,
                    message: response.statusText
                }
                dispatch({type: GEOCODE_ERROR, payload: error})
                throw error
            }
            return response.json()
        })
        .then(response => {
            dispatch({type: GEOCODE_SUCCESS, payload: response.data})
            return response.data
        })
        .catch( error => {
            // log the error
            throw error
        })
    }
}
import {
    ADDRESS_CHANGED,
    SPORT_CHANGED,
    GPS_START,
    GPS_SUCCESS,
    GPS_ERROR,
    REVERSE_GEOCODING_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    address: {
        street: null,
        coord: null
    },
    sport: 'all',
    currentCountry: null,
    currentLocality: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADDRESS_CHANGED:
            return { ...state, address: action.payload};
        case SPORT_CHANGED:
            return { ...state, sport: action.payload};
        case GPS_START:
            return { ...state};
        case GPS_SUCCESS:
            return {
                ...state,
                address: {
                    ...state.address,
                    coord: action.payload
                }
            };
        case GPS_ERROR:
            return {
                ...state,
                address: {
                    street: null,
                    coord: null
                }
            };
        case REVERSE_GEOCODING_SUCCESS:
            return {
                ...state,
                currentCountry: action.payload.country,
                currentLocality: action.payload.locality
            };
        default:
            return state;
    }
};

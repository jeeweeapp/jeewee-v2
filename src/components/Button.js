// import libraries
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Colors } from '../common/Colors';
import EStyleSheet from 'react-native-extended-stylesheet';

EStyleSheet.build();

// make a component
const Button = ({onPress, children, leave, style}) => {
    const { containerStyle, buttonStyle, textStyle, textLeaveStyle, buttonStyleLeave} = styles;
    return (
        <View style={[containerStyle, style]}>
            <TouchableOpacity onPress={onPress} style={leave ? buttonStyleLeave : buttonStyle}>
                <Text style={leave ? textLeaveStyle : textStyle}>
                    {children}
                </Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = {
    containerStyle: {

    },
    buttonStyle: {
        backgroundColor: Colors[Colors.currentTheme].button.primary.bg,
        elevation: 1,
        borderRadius: 30,
        paddingVertical: 15,
        paddingHorizontal: 20,
        marginHorizontal: 20
    },
    buttonStyleLeave: {
        backgroundColor: Colors[Colors.currentTheme].button.leave.bg,
        elevation: 1,
        borderRadius: 30,
        paddingVertical: 15,
        paddingHorizontal: 20,
        marginHorizontal: 20
    },
    textStyle: {
        textAlign: 'center',
        color: Colors[Colors.currentTheme].button.primary.text,
        fontSize: 18,
    },
    textLeaveStyle: {
        color: Colors[Colors.currentTheme].button.leave.text,
        fontSize: 18,
        textAlign: 'center'
    }
};

// make a component available to other parts of the app
export { Button };
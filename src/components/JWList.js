import React, { Component } from 'react';
import { List } from 'react-native-elements';

class JWList extends Component {

    render() {
        return (
            <List containerStyle={styles.list}>
                {this.props.children}
            </List>
        )
    }
}

const styles = {
    list: {
        borderBottomWidth: 0,
        borderTopWidth: 0,
        padding: 0,
        marginTop: 10,
    }
};

// make a component available to other parts of the app
export { JWList };
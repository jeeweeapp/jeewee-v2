import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Header from '../components/Header';
import { List, ListItem, Icon } from 'react-native-elements';
import { Colors } from '../common/Colors';
import { addressVenueChanged, getCurrentLocation, googleMapsGeocode, googleMapsReverseGeocode  } from '../actions';
// limit the List rows to prevent app from crashing
// TODO: we might use some pagination mechanism
const MAX_ROWS = 200;
import {connect} from "react-redux";
import debounce from "lodash/debounce";

class SearchVenue extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            searchResults: null,
            searchQuery: ''
        };

        // debouce prevents to start searches if the user types faster than X milliseconds
        this.startSearch = debounce(this.startSearch, 200);
    }

    componentDidMount() {
        this.props.googleMapsReverseGeocodeDispatch()
        .catch(error => {
            console.log(error)
        })
    }

    updateText(text) {
        this.setState({searchQuery: text}, () => {
            this.startSearch();
        });
    }

    startSearch() {
        if (this.validate(this.state.searchQuery)) {
            this.props.googleMapsGeocodeDispatch(this.state.searchQuery)
                .then(searchResults => {
                    this.setState({searchResults});
                })
        }
    }

    validate(query) {
        // at least 2 words
        return query.split(' ').length > 1
    }

    selectCurrentLocation() {
        if (this.props.gps && this.props.gps.e && this.props.gps.n) {
            // we have already a GPS location, let's use that
            return this.selectLocation({coord: this.props.gps});
        } else {
            this.props.getCurrentLocation()
                .then(() => {
                    return this.selectLocation({coord: this.props.gps});
                })
                .catch(() => {
                    return this.selectLocation(null)
                })
        }
    }

    selectLocation(loc) {
        // loc might be a gps coord or a complete address + coord
        this.props.addressVenueChanged(
            {
                address: {
                    street: loc.street,
                    number: loc.number,
                    zip: loc.zip,
                    city: loc.city
                },
                coord: loc.coord
            }
        );
        this.props.navigation.navigate('Venues');
    }

    renderUseCurrentAddress() {
        return (
            <TouchableHighlight
                underlayColor={'#eee'}
                onPress={this.selectCurrentLocation.bind(this)}
            >
                <View
                    style={styles.useCurrentAddress}
                >
                    <Icon
                        size={25}
                        name='my-location'
                        color={styles.useCurrentAddressIcon.color}
                        iconStyle={styles.useCurrentAddressIcon}
                        containerStyle={styles.useCurrentAddressIconContainer}
                    />
                    <Text
                        style={styles.useCurrentAddressText}
                    >
                        Search around current location
                    </Text>
                </View>
            </TouchableHighlight>
        );
    }

    renderAddresses() {
        if (this.state.searchResults === null) {
            return (
                this.renderUseCurrentAddress()
            )
        } else if (this.state.searchResults.length > 0) {
            if (this.state.searchResults.length > MAX_ROWS) {
                // warning in case of too many results
                return (
                    <Text style={styles.noResult}>
                        There are too many addresses to show.{"\n"}
                        Please try adding the house number to narrow your search...
                    </Text>
                )
            } else {
                // normal result set
                return (
                    <List containerStyle={{marginBottom: 20}}>
                        {
                            this.state.searchResults.map((l, i) => (
                                <ListItem
                                    onPress={
                                        () => {
                                            this.selectLocation(l)
                                        }
                                    }
                                    key={i}
                                    title={`${l.street ? l.street : ''} ${l.number ? l.number : ''} - ${l.zip ? l.zip : ''} ${l.city ? l.city : ''}`}
                                />
                            ))
                        }
                    </List>
                )
            }
        } else if (this.state.searchQuery.length > 0) {
            // warning in case of 0 results
            return <Text style={styles.noResult}
                >No addresses matched your search.{"\n"}
                Try typing a more precise address.</Text>
        } else {
            // the user did't type any character, no need to show the warning
            return true;
        }
    }

    render() {
         return (
             <View>
                 <Header
                    navigation={this.props.navigation}
                     searchQuery={this.state.searchQuery}
                     onChangeText={this.updateText.bind(this)} // TODO: not sure if calling fuctions like this is ok
                     navigateBack={true}
                     placeholder="Type an address to get suggested venues..."
                 />
                 <ScrollView
                     // with these properties clicking on a list item
                     // will directly trigger the onPress instead of dismissing
                     // first the keyboard
                     keyboardDismissMode={'on-drag'}
                     keyboardShouldPersistTaps={'always'}>

                     { this.renderAddresses() }

                 </ScrollView>
             </View>
         );
    }
}

SearchVenue.defaultProps = {
    addressVenue: {
        street: null,
        coord: null
    }
};

const styles = StyleSheet.create({
    noResult: {
        padding: 20,
        backgroundColor: Colors[Colors.currentTheme].warning.bg,
        color: Colors[Colors.currentTheme].warning.text
    },
    useCurrentAddress: {
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc',
        flex: 1,
        flexDirection: 'row'
    },
    useCurrentAddressText: {
        color: Colors[Colors.currentTheme].warning.text,
        flex: 1,
        fontSize: 18,
    },
    useCurrentAddressIconContainer: {
        flex: 0,
        flexBasis: 120,
        flexDirection: 'row'
    },
    useCurrentAddressIcon: {
        flex: 1,
        color: Colors[Colors.currentTheme].icons.color,
    }
});

const mapStateToProps = (state) => {
    const { addressVenue } = state;
    const { gps } = state.location;

    return {
        addressVenue,
        gps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        addressVenueChanged: (address) => {
            dispatch(addressVenueChanged(address));
        },
        getCurrentLocation: () => {
            return dispatch(getCurrentLocation());
        },
        googleMapsGeocodeDispatch: (searchQuery) => {
            return dispatch(googleMapsGeocode(searchQuery))
        },
        googleMapsReverseGeocodeDispatch: () => {
            return dispatch(googleMapsReverseGeocode())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchVenue);
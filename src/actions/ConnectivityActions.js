import {
    OFFLINE,
    SHOW_MESSAGE
} from './types';

import { networkErrorMessage} from '../utils/utils';

export const isOffline = () => {
    return {
        type: OFFLINE
    };
};

export const showOfflineMessage = () => {
    return {
        type: SHOW_MESSAGE,
        payload: networkErrorMessage
    };
};
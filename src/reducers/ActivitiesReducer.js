import {
    ACTIVITIES_GET_START,
    ACTIVITIES_GET_SUCCESS,
    ACTIVITIES_GET_FAIL,

    MY_ACTIVITIES_GET_START,
    MY_ACTIVITIES_GET_SUCCESS,
    MY_ACTIVITIES_GET_FAIL,

    ACTIVITY_CREATE_SUCCESS,
    ACTIVITY_CREATE_UPDATE_FORM,
    ACTIVITY_CREATE_CLEAN_FORM,

    CLEAR_NOTIFICATION
} from '../actions/types';

const INITIAL_STATE = {
    activities: [],
    myActivities: [],
    activityMessage: null,
    form: {
        place: {
            address: null,
            coord: null,
            name: null
        },
        sport: 'running',
        host: null,
        date_start: null,
        date_end: null,
        level: 'any',
        notes: null,
        attendees: [
        ],
        host_id: null
    }
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ACTIVITIES_GET_START:
            return state;
        case ACTIVITIES_GET_SUCCESS:
            return { ...state, activities: action.payload };
        case ACTIVITIES_GET_FAIL:
            return { ...state, activityMessage: action.payload };
        case MY_ACTIVITIES_GET_START:
            return state;
        case MY_ACTIVITIES_GET_SUCCESS:
            return { ...state, myActivities: action.payload };
        case MY_ACTIVITIES_GET_FAIL:
            return state;
        case ACTIVITY_CREATE_SUCCESS:
            return { ...state, activityMessage: action.payload };
        case CLEAR_NOTIFICATION:
            return { ...state, activityMessage: null };
        case ACTIVITY_CREATE_UPDATE_FORM:
            let value = { ...state, form: action.payload};
            return value;
        case ACTIVITY_CREATE_CLEAN_FORM:
            return { ...state, form: INITIAL_STATE.form };
        default:
            return state;
    }
};

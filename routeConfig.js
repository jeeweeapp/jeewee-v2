import Home from './src/scenes/Home';
import SearchAddress from './src/scenes/SearchAddress';
import SearchSport from './src/scenes/SearchSport';
import Activities from './src/scenes/Activities';
import CreateActivity from './src/scenes/CreateActivity';
import MyActivities from './src/scenes/MyActivities';
import SearchVenue from './src/scenes/SearchVenue';
import About from './src/scenes/About';
import Venues from './src/scenes/Venues';

export default {
    Home: { screen: Home },
    SearchAddress: { screen: SearchAddress },
    SearchSport: { screen: SearchSport },
    Activities: { screen: Activities },
    CreateActivity: { screen: CreateActivity },
    MyActivities: { screen: MyActivities },
    SearchVenue: { screen: SearchVenue },
    About: { screen: About },
    Venues: { screen: Venues },
}
// import libraries
import React, {Component} from 'react';
import { Text, View, StyleSheet} from 'react-native';
import { Colors } from '../common/Colors';
import { Spinner } from './Spinner';

class NotificationsHeader extends Component {

    renderSpinner() {
        if (this.props.loading) {
            return (
                <Spinner size="large" />
            );
        }
        return null;
    }

    renderMessage() {
        let style = styles.textStyle;
        if (this.props.layout === 'center') {
            style = styles.textStyleCenter;
        }
        if (this.props.message) {
            return (
                <Text style={style}>
                    { this.props.message }
                </Text>
            )
        }
        return null;
    }

    render() {
        let style = styles.viewStyle;
        if (this.props.layout === 'center') {
            style = styles.viewStyleCenter;
        }
        return (
            <View style={style}>
                {this.renderMessage()}
                {this.renderSpinner()}
            </View>
        )
    }
}

const styles = {
    viewStyle: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        top: 0,
        backgroundColor: 'white',
        right: 0,
        position: 'absolute',
        zIndex: 20
    },
    viewStyleCenter: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
    },
    textStyleCenter: {
        fontSize: 16,
        color: Colors[Colors.currentTheme].notification.text,
        textAlign: 'center',
        padding: 20,
        backgroundColor: Colors[Colors.currentTheme].notification.bg,
        borderRadius: 0,
        lineHeight: 26
    },
    textStyle: {
        fontSize: 14,
        color: Colors[Colors.currentTheme].notification.text,
        flexGrow: 0,
        paddingVertical: 20,
        paddingRight: 20,
        paddingLeft: 20,
        backgroundColor: Colors[Colors.currentTheme].notification.bg,
    }
};

export default NotificationsHeader;
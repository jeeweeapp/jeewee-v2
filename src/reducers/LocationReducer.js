import {
    GPS_START,
    GPS_SUCCESS,
    GPS_ERROR
} from '../actions/types';

const INITIAL_STATE = {
    gps: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GPS_START:
            return state;
        case GPS_SUCCESS:
            return {
                gps: action.payload
            };
        case GPS_ERROR:
            return state;
        default:
            return state;
    }
};

import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableHighlight } from 'react-native';
import Header from '../components/Header';
import { Icon} from 'react-native-elements';
import { NavigationActions } from 'react-navigation';
import { Colors } from '../common/Colors';
import {
    selectVenue,
    getVenues
} from '../actions';
import { JWList } from '../components/JWList';
import VenueListItem from '../components/VenueListItem';
import {connect} from "react-redux";

class Venues extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            venues: [],
            openVenue: null
        };
    }

    componentWillMount() {
        // fetch the closest 10 venues given the chosen address
        this.props.getVenues(this.props.addressVenue, this.props.form.sport, this.props.token);
    }

    renderTitle() {
        return (
            <TouchableHighlight
                onPress={ () => {
                        this.selectAddressOnlyVenue(this.props.addressVenue);
                    }
                }
                underlayColor={'#eee'}
                style={{
                    paddingLeft: 20,
                    padding: 10
                }}
            >
                <Text
                    style={{
                        fontSize: 18,
                        color: Colors[Colors.currentTheme].button.primary.bg
                    }}
                >
                    Use this address
                </Text>
            </TouchableHighlight>
        )
    }

    renderSummary() {
        const { city, street, number } = this.props.addressVenue.address;
        const { coord } = this.props.addressVenue;

        return (
            <View style={styles.summary}>
                <View>
                    <Text style={styles.summaryAddress}>
                        {street ? street : ''} {number ? number : ''} {city ? city : '' }
                    </Text>
                    <Text
                        style={[styles.summaryAddress, { fontSize: 12 }]}
                    >
                        {coord ? 'E: ' + coord.e.toFixed(4) + ', N: ' + coord.n.toFixed(4) : ''}
                    </Text>
                </View>
                <Icon
                    size={24}
                    name='edit'
                    color={styles.summaryIcon.color}
                    iconStyle={styles.summaryIcon}
                    onPress={() => this.props.navigation.dispatch(NavigationActions.back())}
                />
            </View>
        )
    }

    renderVenuesTitle() {
        if (this.props.venues.length === 0) {
            return null
        } else {
            return (
                <Text
                    style={styles.listTitle}
                >
                    Or pick a venue nearby suitable for {this.props.form.sport}:
                </Text>
            )
        }
    }

    selectVenue(venue) {
        this.props.selectVenue(venue);
        this.props.navigation.navigate('CreateActivity');
    }

    selectAddressOnlyVenue(addressVenue) {
        let venue = {
            address: addressVenue.address,
            coord: addressVenue.coord
        };

        this.selectVenue(venue);
    }

    renderVenuesList() {
        if (this.props.venues.length === 0) {
            return null;
        } else {
            return (
                // Keep in mind that ScrollViews must have a bounded height in order to work,
                // since they contain unbounded-height children into a bounded container (via a scroll interaction).
                // In order to bound the height of a ScrollView, either set the height of the
                // view directly (discouraged) or make sure all parent views have bounded height.
                // Forgetting to transfer {flex: 1} down the view stack can lead to errors here,
                // which the element inspector makes easy to debug.
                <ScrollView
                    keyboardShouldPersistTaps={'always'}
                    style={{
                        flex: 1,
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        marginBottom: 10
                    }}
                >
                    <JWList>
                        {
                            this.props.venues.map((venue) => (
                                <VenueListItem
                                    isOpen={this.state.openVenue === venue._id}
                                    key={venue._id}
                                    venue={venue}
                                    onSelect={() => {
                                        this.selectVenue(venue);
                                    }}
                                    onPress={() => {
                                        this.openVenueDetails(venue._id)
                                    }}
                                />
                            ))
                        }
                    </JWList>
                </ScrollView>
            )
        }
    }

    openVenueDetails(venueId) {
        this.setState({openVenue: venueId});
    }

    render() {
         return (
             <View
                 style={{
                    flex: 1,
                 }}
             >
                 <Header
                    navigation={this.props.navigation}
                     navigateBack={true}
                 />
                 <View
                    style={{
                        flex: 1,
                    }}
                 >
                     { this.renderSummary() }

                     { this.renderTitle() }

                     { this.renderVenuesTitle() }

                     { this.renderVenuesList() }
                 </View>
             </View>
         );
    }
}

Venues.defaultProps = {
    addressVenue: {
        address: {
            street: null
        },
        coord: null
    }
};

const styles = StyleSheet.create({
    summary: {
        paddingLeft: 20,
        paddingTop: 20,
        flexDirection: 'row',
        alignItems: 'center'
    },
    summaryIcon: {
        color: Colors[Colors.currentTheme].icons.gray
    },
    summaryAddress: {
        marginRight: 20,
        fontSize: 18
    },
    listTitle: {
        paddingTop: 30,
        marginBottom: 10,
        paddingHorizontal: 20,
    }
});

const mapStateToProps = (state) => {
    const { addressVenue, selectedVenue, venues } = state.venues;
    const { token } = state.auth;
    const { form } = state.activities;

    return {
        addressVenue,
        selectedVenue,
        venues,
        token,
        form
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        selectVenue: (venue) => {
            dispatch(selectVenue(venue));
        },
        getVenues: (addressVenue, sport, token) => {
            dispatch(getVenues(addressVenue, sport, token));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Venues);
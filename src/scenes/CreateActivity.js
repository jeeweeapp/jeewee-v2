import React, { Component } from 'react';
import { View, ScrollView, Text } from 'react-native';
import Header from '../components/Header';
import NotificationsHeader from '../components/NotificationsHeader';
import { SmallButton } from '../components/SmallButton';
import { Button } from '../components/Button';
import { FormInput, FormLabel, Icon} from 'react-native-elements';
import { toTitleCase } from '../utils/utils';
import { Colors } from '../common/Colors';
import JWDatePicker from '../components/JWDatePicker';
import JWTimePicker from '../components/JWTimePicker';
import {
    activityCreate,
    updateForm,
    sportChanged,
    tokenGet,
    activitiesGet
} from '../actions';
import moment from 'moment';
import { connect } from 'react-redux';

const dateFormat = 'ddd, DD MMM';
const timeFormat = 'HH:mm';

class CreateActivity extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            token: null,
            extendSportList: false,
            extraSport: null,
            form: {}
        };
    }

    componentWillMount() {
        let newForm = {};

        if (this.props.userID) {
            // current user is attending the new activity
            if (this.props.form.attendees.length === 0) {
                newForm.attendees = [this.props.userID];
            }
            newForm.host_id = this.props.userID;
        }

        if (this.props.selectedVenue) {
            let formattedPlace = {
                address: this.props.selectedVenue.address,
                coord: this.props.selectedVenue.coord,
                name: this.props.selectedVenue.name
            };
            newForm.place = formattedPlace;
        }

        if (!this.props.form.date_start) {
            let dateStart = moment().hours(12).minutes(0).toISOString();
            newForm.date_start = dateStart;
        }

        if (!this.props.form.date_end) {
            let dateEnd = moment().hours(13).minutes(0).toISOString();
            newForm.date_end = dateEnd;
        }

        if (this.props.sport && this.props.sport !== this.props.form.sport) {
            if (this.props.sport === 'all') {
                newForm.sport = 'running';
            } else {
                newForm.sport = this.props.sport;
            }

            // NOTE: clear the place because it might not be suited for the new sport
            newForm.place = {
                address: null,
                coord: null,
                name: null
            };

            this.selectSport(newForm.sport, false);
        } else {
            this.refreshSportWidget(this.props.form.sport);
        }

        // using spreading to merge two objects
        // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Spread_operator
        // and set the result as new form
        this.props.updateForm(
            {
                ...this.props.form,
                ...newForm
            }
        );
    }

    render() {
        return (
            <View>
                <Header
                    navigation={this.props.navigation}
                    navigateBack={true}
                />
                <NotificationsHeader message={this.props.message} loading={this.props.loading}/>
                <ScrollView
                    // with these properties clicking on a list item
                    // will directly trigger the onPress instead of dismissing
                    // first the keyboard
                    style={{marginBottom: 60}}
                    keyboardShouldPersistTaps={'always'}>

                    { this.renderForm() }

                </ScrollView>
            </View>
        );
    }

    renderTitle() {
        return (
            <Text style={styles.title}>
                New activity
            </Text>
        )
    }

    renderSportList() {
        return (
            <View
                key={'field-sports'}
                style={styles.fieldblock}
            >
                <FormLabel
                    containerStyle={{
                        margin: 0,
                        padding: 0
                    }}
                    labelStyle={styles.label}
                >
                    Sport
                </FormLabel>
                <View
                    key={'sports-buttons-container'}
                    style={styles.sport.buttons}
                >
                    { this.renderDefaultSportsOrSelected() }

                    <SmallButton
                        onWhite={true}
                        key={'other'}
                        onPress={this.props.navigateSearchSport}>
                        {'Other...'}
                    </SmallButton>
                </View>
            </View>
        )
    }

    refreshSportWidget(sport) {
        if (!['running', 'football'].includes(sport)) {
            this.setState(
                {
                    extendSportList: true,
                    extraSport: sport
                }
            );
        }
    }

    selectSport(sport, update = true) {
        this.refreshSportWidget(sport);
        // we update only when manually selecting sport on click
        // not when the component re-renders (cause the form is updated already before)
        // NOTE: clear the place because it might not be suited for the new sport
        if (update) {
            this.props.updateForm({...this.props.form, sport: sport, place: {
                address: null,
                coord: null,
                name: null
            }});
        }

        // TODO: maybe setting the sport in two different places (store and form)
        // is just more work and we could simplify / refactor. Discuss first the business
        // needs to keep search sport and create sport in sync
        this.props.changeGlobalSport(sport);
    }

    selectLevel(level) {
        this.props.updateForm({...this.props.form, level: level});
    }

    renderDefaultSportsOrSelected() {
        if (!this.state.extendSportList) {
            return ([
                <SmallButton
                    key={'small-button-running'}
                    onWhite={true}
                    onPress={() => {this.selectSport('running')}}
                    active={this.props.form.sport === 'running'}>
                    {'Running'}
                </SmallButton>,
                <SmallButton
                    key={'small-button-football'}
                    onWhite={true}
                    onPress={() => {this.selectSport('football')}}
                    active={this.props.form.sport === 'football'}>
                    {'Football'}
                </SmallButton>
            ]);
        } else {
            return (
                <SmallButton
                    key={'small-button-extra'}
                    onWhite={true}
                    onPress={() => {this.selectSport(this.state.extraSport)}}
                    active={this.props.form.sport === this.state.extraSport}>
                    {toTitleCase(this.state.extraSport)}
                </SmallButton>
            )
        }
    }

    renderLocationText() {
        let place = this.props.form.place;
        let currentLocation = 'Around current location';

        if (!place) {
            return currentLocation;
        }

        if (place.name) {
            return place.name;
        }

        if (place.address && place.address.street) {
            if (place.address && place.address.number) {
                return `${place.address.street} ${place.address.number}`;
            }
            return place.address.street;
        }

        return currentLocation;
    }

    renderLocation() {
        return (
            <View
                style={styles.fieldblock}
            >
                <FormLabel
                    labelStyle={styles.label}
                    key={'location-label'}
                >
                    Address or Venue
                </FormLabel>
                <Text
                    onPress={this.props.navigateSearchVenue}
                    style={styles.fakeInput}
                    key={'location-input'}
                >
                    {/* Quite magic. We should also unify the place / address format between search and here */}
                    {this.renderLocationText()}
                </Text>
                <Icon
                    containerStyle={{
                        paddingHorizontal: 0
                    }}
                    iconStyle={{
                        margin: 0
                    }}
                    size={16}
                    name={'place'}
                    color={Colors[Colors.currentTheme].icons.color}
                />
            </View>
        )
    }

    renderHostName() {
        const { form } = this.props;

        return (
            <View
                style={styles.fieldblock}
            >
                <FormLabel
                    labelStyle={styles.label}
                    key={'hostname-label'}
                >
                    Host
                </FormLabel>
                <FormInput
                    autoFocus={false}
                    blurOnSubmit={true}
                    placeholderTextColor={Colors[Colors.currentTheme].input.onWhite.placeholder}
                    placeholder={'(optional) Your name'}
                    underlineColorAndroid={"transparent"}
                    containerStyle={styles.inputContainer}
                    inputStyle={styles.input}
                    value={form.host}
                    onChangeText={(text) => {
                        this.props.updateForm({...form, host: text});
                    }}
                />
            </View>
        )
    }

    renderDateTime() {
        return (
            <View
                style={styles.fieldblock}
            >
                <FormLabel
                    labelStyle={styles.label}
                    key={'datetime-label'}
                >
                    Date / Time
                </FormLabel>
                <JWDatePicker
                    style={{
                        width: 110,
                        marginRight: 5
                    }}
                    date={moment(this.props.form.date_start).format(dateFormat)}
                    onDateChange={(date) => {
                        const { form } = this.props;

                        // Add 2 hours to avoid the datepicker taking midnight
                        // and storing the previous day because of conversion to
                        // UTC time

                        // TODO: 6 hours is europe specific. Make this more solid
                        let newDate = moment(date, dateFormat).add(6, 'h');

                        let newDay = {
                            'year': newDate.year(),
                            'month': newDate.month(),
                            'date': newDate.date()
                        };

                        // change the date keeping the current time
                        let currentDateStart = moment(this.props.form.date_start);
                        let newDateStart = currentDateStart.set(newDay).toISOString();

                        // change the date keeping the current time
                        let currentDateEnd = moment(this.props.form.date_end);
                        let newDateEnd = currentDateEnd.set(newDay).toISOString();

                        this.props.updateForm({...form, date_start: newDateStart, date_end: newDateEnd });
                    }}
                />
                <JWTimePicker
                    date={moment(this.props.form.date_start).format(timeFormat)}
                    style={{
                        width: 65
                    }}
                    onDateChange={(time) => {
                        const { form } = this.props;
                        let momentTime = moment(time, 'HH:mm');
                        let newStartTime = {
                                'hours': momentTime.hours(),
                                'minutes': momentTime.minutes()
                            };

                        // change the time keeping the current date
                        let currentDateStart = moment(form.date_start);
                        let newDateStart = currentDateStart.set(newStartTime).toISOString();
                        this.props.updateForm({...form, date_start: newDateStart});
                    }}
                />
                <Icon
                    containerStyle={{
                        margin: 0,
                        padding: 0,
                    }}
                    iconStyle={{
                        margin: 0
                    }}
                    size={16}
                    name={'arrow-forward'}
                    color={Colors[Colors.currentTheme].icons.color}
                />
                <JWTimePicker
                    date={moment(this.props.form.date_end).format(timeFormat)}
                    end={true}
                    style={{
                        width: 65
                    }}
                    onDateChange={(time) => {
                        const { form } = this.props;
                        let momentTime = moment(time, 'HH:mm');
                        let newEndTime = {
                                'hours': momentTime.hours(),
                                'minutes': momentTime.minutes()
                            };

                        // change the time keeping the current date
                        let currentDateEnd = moment(this.props.form.date_end);
                        let newDateEnd = currentDateEnd.set(newEndTime).toISOString();
                        this.props.updateForm({...form, date_end: newDateEnd});
                    }}
                />
            </View>
        )
    }

    renderLevel() {
        return (
            <View
                style={styles.fieldblock}
            >
                <FormLabel
                    labelStyle={styles.label}
                    key={'datetime-label'}
                >
                    Preferred level
                </FormLabel>
                <SmallButton
                    onWhite={true}
                    key={'level-any'}
                    onPress={() => {this.selectLevel('any')}}
                    active={this.props.form.level === 'any'}>
                    {'Any'}
                </SmallButton>
                <SmallButton
                    onWhite={true}
                    key={'level-beginner'}
                    onPress={() => {this.selectLevel('beginner')}}
                    active={this.props.form.level === 'beginner'}>
                    {'Beginner'}
                </SmallButton>
                <SmallButton
                    onWhite={true}
                    key={'level-average'}
                    onPress={() => {this.selectLevel('average')}}
                    active={this.props.form.level === 'average'}>
                    {'Average'}
                </SmallButton>
                <SmallButton
                    onWhite={true}
                    key={'level-expert'}
                    onPress={() => {this.selectLevel('expert')}}
                    active={this.props.form.level === 'expert'}>
                    {'Expert'}
                </SmallButton>
            </View>
        )
    }

    renderNotes() {
        const { form } = this.props;
        return (
            <View
                style={styles.fieldblock}
            >
                <FormLabel
                    labelStyle={[styles.label, { flex: 1, alignSelf: 'flex-start'}]}
                    key={'notes-label'}
                >
                    Notes
                </FormLabel>
                <FormInput
                    multiline={true}
                    value={form.notes}
                    placeholderTextColor={Colors[Colors.currentTheme].input.onWhite.placeholder}
                    placeholder={'(optional)\nMore info can always help guests to find and join you!'}
                    underlineColorAndroid={"transparent"}
                    textAlignVertical={'top'}
                    containerStyle={
                        [
                            styles.inputContainer,
                            {
                                borderBottomWidth: 0
                            }
                        ]
                    }
                    inputStyle={[styles.input, {fontSize: 14, height: 60, textAlignVertical: 'top', paddingTop: 0, maxWidth: '100%' }]}
                    onChangeText={(text) => {
                        this.props.updateForm({...this.props.form, notes: text});
                    }}
                />
            </View>
        )
    }

    renderSubmit() {
        return (
            <Button
                style={{marginTop: 30}}
                onPress={() => {
                    this.props.submitForm(this.props.form, this.props.token)
                        .then(() => {
                            // refresh the activities before going back to the activities list
                            // this is because react navigator does not re-render the components
                            // (it does not call didmount when navigating!)
                            // There are some other ways https://github.com/react-navigation/react-navigation/issues/51
                            // but they look more complicated
                            return this.props.activitiesGet();
                        })
                        .then(() => {
                            return this.props.navigation.navigate('Activities')
                        })
                }}
            >
                Create activity
            </Button>
        )
    }

    renderForm() {
        return (
            <View style={styles.container}>
                { this.renderTitle() }

                { this.renderSportList() }

                { this.renderLocation() }

                { this.renderDateTime() }

                { this.renderLevel() }

                { this.renderHostName() }

                { this.renderNotes() }

                { this.renderSubmit() }

            </View>
        )
    }
}

CreateActivity.defaultProps = {
    token: 'Missing token',
    form: {
        place: {
            address: null,
            coord: null,
            name: null
        },
        sport: 'running',
        host: null,
        date_start: moment().add(1, 'h').toISOString(),
        date_end: moment().add(2, 'h').toISOString(),
        level: 'any',
        notes: null,
        attendees: [
        ],
        host_id: null
    }
};

const styles = {
    container: {
        padding: 15
    },
    title: {
        fontSize: 22,
        color: '#222',
        marginBottom: 0
    },
    fieldblock: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10
    },
    fakeInput: {
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 16,
        color: Colors[Colors.currentTheme].input.onWhite.text,
        paddingHorizontal: 0,
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderBottomColor: Colors[Colors.currentTheme].input.onWhite.border,
    },
    label: {
        width: 60,
        marginLeft: 0,
        marginRight: 5,
        marginTop: 0,
        marginBottom: 0,
        textAlign: 'right'
    },
    inputContainer: {
        flex: 1,
        paddingLeft: 0,
        paddingBottom: 0,
        marginLeft: 10,
        marginBottom: 0,
        borderBottomWidth: 1,
        borderBottomColor: Colors[Colors.currentTheme].input.onWhite.border
    },
    input: {
        height: 35,
        fontSize: 16,
        paddingVertical: 5,
        paddingLeft: 0,
        color: Colors[Colors.currentTheme].input.onWhite.text
    },
    sport: {
        buttons: {
            padding: 0,
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            flexDirection:'row',
            marginBottom: 0
        }
    }
};


const mapStateToProps = (state) => {
    const { auth, notifications, search, venues, activities} = state;
    const {
        token,
        userID
    } = auth;

    const { sport } = search; // used when searching for a sport

    const { selectedVenue } = venues;

    const { form } = activities;

    const {
        message,
        loading
    } = notifications;

    return {
        loading,
        token,
        message,
        sport,
        userID,
        selectedVenue,
        form
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        activitiesGet: () => {
            return dispatch(activitiesGet());
        },
        tokenGet: () => {
            dispatch(tokenGet());
        },
        updateForm: (form) => {
            dispatch(updateForm(form));
        },
        submitForm: (form, token) => {
            return dispatch(activityCreate(form, token));
        },
        navigateSearchSport: () => {
            ownProps.navigation.navigate('SearchSport')
        },
        navigateSearchVenue: () => {
            ownProps.navigation.navigate('SearchVenue')
        },
        // because the search sport and create search sport
        // has to be the same (unless we refactor), anytime we
        // update the current form sport we need to update it globally
        // in the store as well. Otherwise, we end up with a wrong config
        // after coming back to the form after searching for a venue
        changeGlobalSport: (sport) => {
            dispatch(sportChanged(sport));
        }
    }
};


// wire Home to Store and wire the actions
export default connect(mapStateToProps, mapDispatchToProps)(CreateActivity);
import { DrawerNavigator } from 'react-navigation';
import routeConfig from './routeConfig';
import JWDrawer from './src/components/JWDrawer';
export default DrawerNavigator(
    routeConfig,
    {
        headerMode: 'none',
        contentComponent: JWDrawer
    }
);
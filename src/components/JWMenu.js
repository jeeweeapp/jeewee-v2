// import libraries
import React from 'react';
import { Text, View, TouchableHighlight} from 'react-native';
import { Colors } from '../common/Colors';
import { NavigationActions } from 'react-navigation';

const backHome = NavigationActions.back({
    key: 'Home'
})

// make a component
const JWMenu = ({navigation}) => (
    <View style={styles.jwmenu}>
        <Text style={styles.jwmenutitle}>
            MENU
        </Text>
        <View
            style={styles.jwmenucontent}
        >
            <TouchableHighlight
                style={styles.jwmenucontentbutton}
                onPress={() => {
                    navigation.dispatch(backHome);
                }}
                underlayColor={Colors[Colors.currentTheme].button.primary.bg}
            >
                <Text style={styles.jwmenucontenttext}>Home</Text>
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                style={styles.jwmenucontentbutton}
                onPress={() => navigation.navigate('MyActivities')}
            >
                <Text style={styles.jwmenucontenttext}>My activities</Text>
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                style={styles.jwmenucontentbutton}
                onPress={() => navigation.navigate('About')}
            >
                <Text style={styles.jwmenucontenttext}>About Jeewee</Text>
            </TouchableHighlight>
        </View>
    </View>
);

const styles = {
    jwmenu: {
        flex: 1,
        backgroundColor: Colors[Colors.currentTheme].menu.bg,
        padding: 0,
        borderRightWidth: 1,
        borderRightColor: Colors[Colors.currentTheme].menu.border,
    },
    jwmenutitle: {
        color: Colors[Colors.currentTheme].menu.text,
        fontSize: 22,
        padding: 20,
        paddingTop: 15
    },
    jwmenucontent: {
        marginTop: 40
    },
    jwmenucontentbutton: {
        margin: 0,
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: Colors[Colors.currentTheme].menu.border
    },
    jwmenucontenttext: {
        fontSize: 18,
        color: Colors[Colors.currentTheme].menu.links
    },
};

// wire Home to Store and wire the actions
export default JWMenu;
import React, { Component } from 'react';
import DatePicker from 'react-native-datepicker';
import {Colors} from '../common/Colors';

const timeFormat = 'HH:mm';

export default class JWTimePicker extends Component {
    render(){
        return (
            <DatePicker
                showIcon={false}
                style={this.props.style}
                date={this.props.date}
                mode="time"
                placeholder="select date"
                format={timeFormat}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateInput: {
                        paddingRight: 0,
                        marginLeft: 0,
                        marginRight: 0,
                        borderWidth: 0,
                        alignItems: 'flex-start',
                    },
                    dateText: {
                        fontSize: 16,
                        padding: 10,
                        marginRight: 0,
                        color: Colors[Colors.currentTheme].input.onWhite.text
                    }
                }}
                onDateChange={this.props.onDateChange}
            />
        )
    }
}
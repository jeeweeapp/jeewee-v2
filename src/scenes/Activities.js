import React, { Component } from 'react'
import { View, ScrollView, Text } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Header from '../components/Header'
import { JWList } from '../components/JWList'
import { Colors } from '../common/Colors'
import { Icon } from 'react-native-elements'
import { Button } from '../components/Button'
import ActivityListItem from '../components/ActivityListItem'
import { toTitleCase } from '../utils/utils'
import {
    activitiesGet,
    activityJoin,
    activityLeave,
    clearNotification
} from '../actions/index'
// redux
import { connect } from 'react-redux'

EStyleSheet.build()

class Activities extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            activities: [],
            openActivity: null
        };
    }

    componentWillMount() {
        this.props.clearNotification(this.props.activityMessage);
        this.props.activitiesGet();
    }

    componentWillUnmount() {
        this.props.clearNotification(true, 1);
    }

    renderNotifications() {
        if (this.props.activityMessage) {
            return (
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        padding: 10,
                        paddingLeft: 20,
                        backgroundColor: Colors[Colors.currentTheme].notification.success.bg
                    }}
                >
                    <Icon
                        size={24}
                        name='done'
                        color={Colors[Colors.currentTheme].notification.success.text}
                        iconStyle={{
                            flex: 1,
                            marginRight: 10
                        }}
                    />
                    <Text
                        style={{
                            flex: 1,
                            fontSize: 15,
                            color: Colors[Colors.currentTheme].notification.success.text
                        }}
                    >
                        { this.props.activityMessage }
                    </Text>
                </View>
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                    <Header
                        navigation={this.props.navigation}
                    />
                    { this.renderNotifications() }
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: 'white'
                        }}
                        // with these properties clicking on a list item
                        // will directly trigger the onPress instead of dismissing
                        // first the keyboard
                        keyboardDismissMode={'on-drag'}
                        keyboardShouldPersistTaps={'always'}>

                        { this.renderActivities() }

                        {this.renderCreateActivityIcon()}

                    </View>
            </View>
        );
    }

    renderResults() {
        if (this.props.activities.length === 0) {
            return (
                <View>
                    <Text style={styles.noResultsMessage}>
                        {'Whoa! Looks like there are no activities yet of this type'}
                    </Text>
                    <Button
                        onPress={() => {
                            this.props.navigation.navigate('CreateActivity');
                        }}
                    >
                    Create new activity !
                    </Button>
                </View>
            )
        } else {
            return (
                <ScrollView
                    style={{
                        borderWidth: 0,
                        paddingHorizontal: 0,
                        paddingVertical: 0,
                        margin: 0,
                        marginBottom: 80
                    }}
                >
                    <JWList>
                        {
                            this.props.activities.map((activity, index) => (
                                <ActivityListItem
                                    userID={this.props.userID}
                                    isOpen={this.state.openActivity === activity._id}
                                    key={activity._id}
                                    listIndex={index}
                                    activity={activity}
                                    onPress={() => {
                                        this.openActivityDetails(activity._id)
                                    }}
                                    onJoin={() => {
                                        var that = this;
                                        this.props.activityJoin(activity, this.props.token)
                                        .then(function(){
                                            // get the actvities again after joining
                                            // to refresh the view
                                            that.props.activitiesGet()
                                        })
                                    }}
                                    onLeave={() => {
                                        var that = this;
                                        this.props.activityLeave(activity, this.props.token)
                                        .then(function(){
                                            // get the actvities again after joining
                                            // to refresh the view
                                            that.props.activitiesGet()
                                        })
                                    }}
                                />
                            ))
                        }
                    </JWList>
                </ScrollView>
            )
        }

    }

    renderCreateActivityIcon(){
        if (this.props.token){
            return (
                <Icon
                    size={25}
                    name='create'
                    color={styles.iconCreateStyle.color}
                    iconStyle={styles.iconCreateStyle}
                    containerStyle={styles.iconCreateContainerStyle}
                    onPress={() => this.props.navigation.navigate('CreateActivity')}
                    raised={true}
                />
            )
        } else {
            return null;
        }
    }

    renderActivities() {
        return (
            <View style={{backgroundColor: 'white'}}>
                <View style={styles.searchInfo}>
                    <Text style={styles.searchInfoText}>
                        {toTitleCase(this.props.sport) + ' around ' + (this.props.address.street || 'you')}
                    </Text>
                    <Icon
                        size={30}
                        name='edit'
                        color={styles.iconStyle.color}
                        containerStyle={{
                            height: 30
                        }}
                        iconStyle={styles.iconStyle}
                        onPress={() => this.props.navigation.navigate('Home')}
                    />
                </View>
                { this.renderResults()}
            </View>
        )
    }

    openActivityDetails(activityId) {
        this.setState({openActivity: activityId});
    }
}

const styles = EStyleSheet.create({
    iconStyle: {
        flex: 1,
        marginLeft: 15,
        padding: 0,
        color: Colors[Colors.currentTheme].icons.clickable
    },
    searchInfo: {
        flexDirection: 'row',
        padding: 0,
        paddingLeft: 20,
        paddingTop: 20,
        paddingBottom: 10,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    searchInfoText: {
        color: '#222',
        fontSize: 18,
        flex: 0,
        maxWidth: '100% - 75',
        flexWrap: 'wrap',
        padding: 0,
        margin: 0,
    },
    noResultsMessage: {
        padding: 20,
        fontStyle: 'italic',
        color: Colors[Colors.currentTheme].noResults.text,
        marginBottom: 40,
        fontSize: 18
    },
    placeIconStyle: {
        flex: 0,
        marginLeft: 0,
        marginRight: 5,
        padding: 0,
        color: Colors[Colors.currentTheme].icons.color
    },
    iconCreateStyle: {
        color: Colors[Colors.currentTheme].button.primary.text
    },
    iconCreateContainerStyle: {
        backgroundColor: Colors[Colors.currentTheme].button.primary.bg,
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 100
    }
});

const mapStateToProps = (state) => {
    const { token, userID } = state.auth;
    const { sport, address } = state.search;
    const { activities, activityMessage } = state.activities;
    return { sport, address, activities, activityMessage, token, userID};
};

const mapDispatchToProps = (dispatch) => {
    return {
        activitiesGet: () => {
            return dispatch(activitiesGet());
        },
        activityJoin: (activity, token) => {
            return dispatch(activityJoin(activity, token));
        },
        activityLeave: (activity, token) => {
            return dispatch(activityLeave(activity, token));
        },
        clearNotification: (enable, delay) => {
            if (enable) {
                setTimeout(function () { //eslint-disable-line
                    dispatch(clearNotification());
                }, delay || 6000);
            }
        }
    }
};


// wire Home to Store and wire the actions
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Activities);
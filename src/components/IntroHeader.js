import React, {Component} from 'react';
import { View, Image } from 'react-native';

class IntroHeader extends Component {
    render() {
        const {containerStyle, imageStyle} = styles;
        return (
                <View
                    style={containerStyle}
                >
                    <Image
                        resizeMode={Image.resizeMode.contain}
                        source={require('../images/logo.png')}
                        style={imageStyle} />
                </View>
            )
    }
}

const styles = {
    containerStyle: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        paddingTop: 90,
        paddingBottom: 0,
    },
    imageStyle: {
        height: 50,
        width: undefined,
    }
};

// make a component available to other parts of the app
export { IntroHeader };
import React, { Component } from 'react';
import { View, ScrollView, Text, StyleSheet, TouchableHighlight} from 'react-native';
import Header from '../components/Header';
import { Colors } from '../common/Colors';
import { openLink } from '../utils/utils';

const emailLink = {
    url: 'mailto:hello@jeeweeapp.com?subject=Jeewee app feedback',
    text: 'hello@jeeweeapp.com'
};

const formLink = {
    url: 'https://goo.gl/forms/zUOx3BrjgIfgzVqk2',
    text: 'Feedback Form'
};

class About extends Component {
    render() {
        return (
            <View>
                <Header
                    navigation={this.props.navigation}
                    navigateBack={true}
                />
                <ScrollView
                    style={{
                        marginTop: 20,
                        marginHorizontal: 10
                    }}
                    contentContainerStyle={{
                        alignItems: 'flex-start'
                    }}
                    // with these properties clicking on a list item
                    // will directly trigger the onPress instead of dismissing
                    // first the keyboard
                    keyboardDismissMode={'on-drag'}
                    keyboardShouldPersistTaps={'always'}>

                    <Text
                        style={styles.aboutTitle}
                    >
                        About Jeewee
                    </Text>

                    <Text
                        style={styles.aboutText}
                    >
                        {"Jeewee is a startup made in Luxembourg." +
                        "\n\nOur mission is to connect people through sport activities."}
                    </Text>
                    <Text
                        style={styles.aboutText}
                    >
                        {"Please don't hesitate to send us any feedback:"}
                    </Text>
                    <TouchableHighlight
                        underlayColor={Colors[Colors.currentTheme].button.onWhite.link.textActive}
                        onPress={() => {
                            openLink(emailLink.url)
                        }}
                    >
                        <Text
                            style={styles.aboutEmail}
                        >
                            {emailLink.text}
                        </Text>
                    </TouchableHighlight>
                    <Text
                        style={styles.aboutText}
                    >
                        Got 1 minute to help us improve ? Please fill in the following form:
                    </Text>
                    <TouchableHighlight
                        underlayColor={Colors[Colors.currentTheme].button.onWhite.link.textActive}
                        onPress={() => {
                            openLink(formLink.url)
                        }}
                    >
                        <Text
                            style={styles.aboutEmail}
                        >
                            {formLink.text}
                        </Text>
                    </TouchableHighlight>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    aboutTitle: {
        flex: 1,
        marginHorizontal: 10,
        marginVertical: 5,
        fontSize: 22,
        maxWidth: 300,
        color: '#222'
    },
    aboutText: {
        flex: 1,
        margin: 10,
        maxWidth: 300
    },
    aboutEmail: {
        marginHorizontal: 10,
        color: Colors[Colors.currentTheme].button.onWhite.link.text,
        fontSize: 18,
    }
});

export default About;
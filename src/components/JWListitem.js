import React, { Component } from 'react';
import { ListItem } from 'react-native-elements';

class JWListItem extends Component {

    render() {
        return (
            <ListItem/>
        )
    }
}

const styles = {
    listItem: {
        borderBottomWidth: 0,
        borderTopWidth: 0,
        backgroundColor: 'red'
    }
};

// make a component available to other parts of the app
export { JWListItem };
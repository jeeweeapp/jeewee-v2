'use strict';

import {filterByPropNameWithRegex} from './utils';

const ADDRESS_KEY = 'rue';
const NUMBER_KEY = 'number';

function compare(a, b) {
    return a[NUMBER_KEY] - b[NUMBER_KEY];
}

/**
 *
 * @param i the current char index
 * @param address the address string
 * @returns {boolean} Whether the char at index i comes after a number or not
 */
function isAddressLetter(i, address){
    let char = address[i];
    return (['a', 'b', 'c', 'd', 'e'].indexOf(char) > -1) && !isNaN(parseInt(address[i-1]));
}

/**
 *
 * Given a string, if it contains a number, that number
 * is returned separately to make a better query.
 *
 * @param address The input string
 */
function separateNumber(address) {
    let i, len;
    let number = '';
    for (i = 0, len = address.length; i < len; i++) {
        let char = address[i];
        if (!isNaN(parseInt(char)) || isAddressLetter(i, address)) {
            // store the numeric char on a separate string
            number += address[i];
        }
    }
    // remove the char from the original string
    address = address.replace(number, '');
    // cleanup trailing spaces
    address = address.trim();
    // append the number at the end of the original string
    let obj = {};
    obj[ADDRESS_KEY] = address;
    obj[NUMBER_KEY] = number;
    return obj;
}

module.exports = {
    // TODO: check if we can do better than O(n)
    /**
     * @param {object} addresses[] - The list of addresses where to search
     * @param {string} needle - The piece of street to search for
     *
     * @property {string} rue - The street of an address
     */
    searchAddress: function(addresses, needle) {
        let result = [];
        let tempResult = [];
        if (needle.length > 2) {
            let _needleObj = separateNumber(needle.toLowerCase());

            tempResult = filterByPropNameWithRegex(addresses, _needleObj[ADDRESS_KEY], ADDRESS_KEY);

            // after filtering by address, filter again by number if number is provided
            if (tempResult.length > 1 && _needleObj[NUMBER_KEY].length > 0) {

                result = filterByPropNameWithRegex(tempResult, _needleObj[NUMBER_KEY], NUMBER_KEY);

                if (result.length > 1) {
                    // TODO: would be better to group by street name and then sort by number
                    result.sort(compare);
                } else if (result.length === 0) {
                    // the number was not found, so at least return the all list of addresses
                    result = tempResult;
                }

            } else {
                result = tempResult;
            }
        }
        return result;
    }
};
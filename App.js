// import a library to help create component
import React from 'react';
import { addNavigationHelpers } from 'react-navigation';
import { Provider, connect } from "react-redux";
import configureStore from './src/store/configureStore';
import Config from 'react-native-config';
import { Colors } from './src/common/Colors';
import {
    StyleSheet,
    View,
    StatusBar,
    Platform
} from 'react-native';

import {
    GoogleAnalyticsTracker
} from 'react-native-google-analytics-bridge';

import AppNavigator from './AppNavigator';

const store = configureStore();

let tracker1 = new GoogleAnalyticsTracker(Config.GOOGLE_ANALYTICS);
tracker1.allowIDFA(true);
tracker1.trackScreenView('OpenApp');
tracker1.trackEvent('OpenAppCategory', 'OpenAppAction');

const NavWrapper = ({dispatch, navigation}) => (
    <AppNavigator navigation={addNavigationHelpers({
        dispatch: dispatch,
        state: navigation,
    })} />
);

const mapStateToProps = (state) => ({
    navigation: state.navigation
});
  
const AppWithNavigationState = connect(mapStateToProps)(NavWrapper);

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

const App = () => (
    <Provider store={store}>
        <View style={styles.container}>
            <MyStatusBar backgroundColor={Colors[Colors.currentTheme].header.bg} barStyle="light-content" />
            <AppWithNavigationState />
        </View>
    </Provider>
)

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    }
});

export default App;

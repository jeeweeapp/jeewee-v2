import React from 'react'
import { View, StyleSheet, Linking, TouchableHighlight, TouchableOpacity, Image, Platform } from 'react-native'
import { Icon, Text, normalize} from 'react-native-elements'
import moment from 'moment';
import { Colors } from '../common/Colors';
import { Button } from '../components/Button';
import { SmallButton } from '../components/SmallButton';
import { toTitleCase, openLink } from '../utils/utils';

const timeFormat = 'HH:mm';
const googleMapsLink = 'http://www.google.com/maps/place/';

// custom config to show only the day without time
moment.updateLocale('en', {
    calendar: {
        sameDay: function () {
            return '[Today]';
        },
        nextDay: function() {
            return '[Tomorrow]'
        },
        nextWeek: function() {
            // only the day of the week
            return 'dddd'
        },
        sameElse: function() {
            // only the day/month
            return 'D MMM'
        }
    }
});

const ActivityListItem = ({
    userID,
    onPress,
    isOpen,
    activity,
    listIndex,
    onJoin,
    onLeave
}) => {
    let Component = TouchableOpacity;
    const {sport, date_start, date_end, level, distance, place, notes, host_name, attendees, host_id} = activity;

    let renderJoinButton = () => {
        if (attendees.indexOf(userID) < 0) {
            return (
                <Button
                    onPress={onJoin}
                >
                    {'Join'}
                </Button>
            )
        } else {
            return (
                <SmallButton
                    style={{
                        alignSelf: 'center'
                    }}
                    onWhite={true}
                    onPress={onLeave}
                >
                    {'Leave'}
                </SmallButton>
            )
        }
    }

    let renderNotes = () => {
        if (notes) {
            return (
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style={{
                            flex: 0,
                            fontStyle: 'italic'
                        }}
                    >
                        {'Notes: '}
                    </Text>
                    <Text
                        style={{
                            fontStyle: 'italic',
                            // TODO: use %
                            maxWidth: 200
                        }}
                    >
                        {notes}
                    </Text>
                </View>
            )
        } else {
            return null;
        }
    }

    let renderHost = () => {
        return (
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    marginBottom: 5
                }}
            >
                <Text
                    style={{
                        flex: 0,
                        fontStyle: 'italic'
                    }}
                >
                    {'Hosted by: '}
                </Text>
                <Text
                    style={{
                            fontStyle: 'italic',
                        }}
                >
                    {host_id === userID ? 'Me as ' : ''} {host_name || 'Anonymous'}
                </Text>
            </View>
        )
    }

    let renderWhere = () => {
        if (!place.address) {
            return (
                <View
                    onPress={() => {
                            openLink(googleMapsLink + place.coord.n + ',' + place.coord.e)
                        }
                    }
                    style={{
                        flexDirection: 'row',
                        alignItems: 'flex-end'
                    }}
                >
                    <Text
                        style={{
                            marginLeft: 10,
                            paddingRight: 5,
                            paddingBottom: 2,
                            borderBottomWidth: 1,
                            borderBottomColor: '#ccc',
                        }}
                    >
                        Open in maps
                    </Text>
                    <Icon
                        iconStyle={{
                            flex: 1,
                            paddingBottom: 2,
                            borderBottomWidth: 1,
                            borderBottomColor: '#ccc',
                        }}
                        name={'place'}
                        color={Colors[Colors.currentTheme].icons.color}
                        size={20}
                    />
                </View>
            )
        } else {
            return (
                <View
                    style={{
                        flex: 0,
                        marginLeft: 10
                    }}
                >
                    <Text>
                        { place.name ? place.name + ' ' : ''}
                    </Text>
                    <Text
                        style={{
                            fontStyle: 'italic',
                            textDecorationLine: 'underline'
                        }}
                    >
                        {place.address.street} {place.address.number} { place.address.city ? ' - ' + place.address.city : ''}
                    </Text>
                </View>
            )
        }
    };

    let renderOpen = () => {
        if (isOpen) {
            return (
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        marginHorizontal: 10,
                        marginVertical: 20,
                        justifyContent: 'flex-start'
                    }}
                >
                    <TouchableHighlight
                        underlayColor={'white'}
                        onPress={() => {
                                openLink(googleMapsLink + place.coord.n + ',' + place.coord.e)
                            }
                        }
                    >
                        <View
                            style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginBottom: 5
                        }}
                        >
                            <Text
                                style={{
                                    flex: 0,
                                    fontStyle: 'italic'
                                }}
                            >
                                {'Where: '}
                            </Text>
                            { renderWhere() }
                        </View>
                    </TouchableHighlight>

                    { renderHost() }

                    { renderNotes() }

                    <View
                        style={{
                            marginTop: 20
                        }}
                    >
                        {renderJoinButton()}
                    </View>
                </View>
            )
        } else {
            return null;
        }
    };

    let attendeesIconColor = Colors[Colors.currentTheme].icons.gray;

    if (attendees.length > 1) {
        attendeesIconColor = Colors[Colors.currentTheme].icons.color;
    }

    let renderAttendees = () => {
        return (
            <View
                style={{
                    marginLeft: 5,
                    flex: 0,
                    flexDirection: 'row',
                    alignItems: 'center'
                }}
            >
                <Text
                    style={{
                        flex: 0,
                        marginRight: 5,
                        textAlign: 'right'
                    }}
                >
                    { attendees.length }
                </Text>
                <Icon
                    containerStyle={{
                        flex: 0,
                        padding: 0,
                        margin: 0
                    }}
                    iconStyle={{
                        padding: 0,
                        marginLeft: 0,
                        marginRight: 7,
                        marginBottom: 0,
                    }}
                    size={20}
                    name={'people'}
                    color={attendeesIconColor}
                />
            </View>
        )
    };

    let hostIcon = null;

    if (host_id === userID) {
        hostIcon = <Icon
            name={'home'}
            size={14}
            color={Colors[Colors.currentTheme].icons.darkGray}
            containerStyle={{
                marginLeft: 5
            }}

        />
    }

    return (
        <Component
            underlayColor={'transparent'}
            onPress={isOpen ? null : onPress}
            style={[
                isOpen ? styles.containerOpen : styles.container,
                listIndex === 0 ? {
                    paddingTop: 0
                } : ''
            ]}>
            <View
                style={{
                    flex: 1,
                    paddingHorizontal: 10
                }}
            >
                <View style={styles.wrapper}>
                    { renderAttendees() }
                    <View style={styles.titleContainer}>
                        <View
                            style={{
                                flexDirection: 'row'
                            }}
                        >
                            <Text
                                style={[
                                  styles.title
                                ]}>
                                    {toTitleCase(sport)}
                            </Text>
                            {hostIcon}
                        </View>
                        <View
                            style={{
                                marginTop: 2,
                                flexDirection: 'row',
                                justifyContent: 'flex-start',
                                alignItems: 'flex-end'
                            }}
                        >
                            <Text
                                style={[
                                    styles.subtitle
                                ]}>
                                {
                                    moment(date_start).calendar()
                                }
                            </Text>
                            <Icon
                                iconStyle={{
                                    marginLeft: 5,
                                    marginRight: 5,
                                    marginBottom: 2,
                                }}
                                size={12}
                                name={'schedule'}
                                color={Colors[Colors.currentTheme].icons.color}
                            />
                            <Text>
                                {
                                    moment(date_start).format(timeFormat)
                                }
                                <Text
                                    style={{
                                        color: '#ccc'
                                    }}
                                >
                                    {' - '}
                                </Text>
                                {
                                    moment(date_end).format(timeFormat)
                                }

                            </Text>
                        </View>
                    </View>
                    <View style={styles.rightContentContainer}>
                        <View style={{
                            flexDirection: 'row',
                            marginBottom: 3
                        }}>
                            <Icon
                                iconStyle={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                    }}
                                size={16}
                                name={'place'}
                                color={Colors[Colors.currentTheme].icons.color}
                            />
                            <Text
                                style={{
                                    fontSize: 14,
                                    alignSelf: 'flex-end',
                                }}
                            >
                                {distance}
                            </Text>
                            <Text
                                style={{
                                    alignSelf: 'flex-end',
                                    fontSize: 12,
                                    marginLeft: 2,
                                    marginBottom: 1,
                                    color: '#aaa',
                                }}
                            >
                                {'Km'}
                            </Text>
                        </View>
                        <View style={{
                            flexDirection: 'row'
                        }}>
                            <Icon
                                iconStyle={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                    }}
                                size={16}
                                name={
                                    level === 'average' ?
                                        'brightness-medium' : level === 'expert' ?
                                        'brightness-high' : level === 'beginner' ?
                                        'brightness-low' : 'brightness-auto'
                                }
                                color={Colors[Colors.currentTheme].icons.color}
                            />
                            <Text
                                style={{
                                    fontSize: 14,
                                }}
                            >
                                {toTitleCase(level)}
                            </Text>
                        </View>
                    </View>
                </View>
                {renderOpen()}
            </View>
        </Component>
    )
};

ActivityListItem.defaultProps = {
    underlayColor: 'white'
};

const styles = StyleSheet.create({
    container: {
        marginLeft: 0,
        paddingTop: 10,
        paddingRight: 0,
        paddingBottom: 10,
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        backgroundColor: 'transparent'
    },
    containerOpen: {
        marginLeft: 0,
        paddingTop: 10,
        paddingRight: 0,
        paddingBottom: 10,
        borderBottomColor: '#ededed',
        borderBottomWidth: 1,
        backgroundColor: 'transparent',
        minHeight: 150
    },
    wrapper: {
        flexDirection: 'row'
    },
    icon: {
        marginRight: 8
    },
    title: {
        fontSize: normalize(14),
        color: '#222',
    },
    subtitle: {
        fontSize: normalize(12),
        marginTop: 1,
        width: 72
    },
    titleContainer: {
        justifyContent: 'center',
        flex: 1,
        marginLeft: 5
    },
    rightContentContainer: {
        flex: 0,
        width: 95,
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        margin: 0
    },
    rightTitleContainer: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    rightTitleStyle: {
        marginRight: 5,
    }
});

export default ActivityListItem

import React, { Component } from 'react';
import { View, Text, Image, NetInfo, Platform} from 'react-native';
import {Icon} from 'react-native-elements';

import { IntroHeader } from '../components/IntroHeader';
import { SmallButton } from '../components/SmallButton';
import { Button } from '../components/Button';
import { Colors } from '../common/Colors';

// redux
import { connect } from 'react-redux';
import NotificationsHeader from '../components/NotificationsHeader';

import {
    tokenGet,
    addressChanged,
    sportChanged,
    getCurrentLocation,
    isOffline,
    showOfflineMessage,
    clearNotification
} from '../actions';

import { toTitleCase } from '../utils/utils';

export function isNetworkConnected() {
    if (Platform.OS === 'ios') {
        return new Promise(resolve => {
            const handleFirstConnectivityChangeIOS = isConnected => {
                NetInfo.isConnected.removeEventListener('connectionChange', handleFirstConnectivityChangeIOS);
                resolve(isConnected);
            };
            NetInfo.isConnected.addEventListener('connectionChange', handleFirstConnectivityChangeIOS);
        });
    }

    return NetInfo.isConnected.fetch();
  }

class Home extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            token: null,
            loading: false,
            message: null,
            sport: 'all',
            address: {
                street: null,
                coord: null
            },
            extendSportList: false,
            extraSport: null
        };
    }



    componentDidMount() {
        this.props.clearNofication(this.props.message);

        // simple connectivity check
        // See also https://github.com/facebook/react-native/issues/8615
        isNetworkConnected().then((isConnected) => {
            if (!isConnected) {
                this.props.isOffline();
            } else {
                if (!this.props.token) {
                    this.props.tokenGet();
                }
                this.selectSport(this.props.sport);
            }
        })
    }

    render() {
        if (this.props.token) {
            return (
                <View style={{
                    backgroundColor: Colors[Colors.currentTheme].home.bg,
                    flex: 1,
                    justifyContent: 'space-between'
                }}>

                    {this.renderMenuIcon()}

                    <View style={{
                        flexDirection: 'column',
                        justifyContent: 'space-between'
                    }}>
                        <IntroHeader />
                        <View style={{
                            zIndex: 2
                        }}>
                            <NotificationsHeader layout={'center'} message={this.props.message} loading={this.props.loading}/>
                            {this.renderContent()}
                        </View>
                    </View>
                    <Image
                        source={require('../images/home-fluo.jpg')}
                        style={{
                            height: 210,
                            bottom: 2,
                            width: undefined,
                        }}
                        resizeMode="cover"
                    />

                    {this.renderCreateActivityIcon()}

                </View>
            )
        } else {
            return (
                <View>
                    <Text>
                        Loading...
                    </Text>
                </View>
            )
        }
    }

    renderContent() {
        const { fakeInput, fakeInputText, fakeInputTextDull, smallButtonContainer } = styles;
        if (this.props.token) {
            return ([
                <Text
                    key={'fakeInput'}
                    style={fakeInput}
                    onPress={() => {this.props.navigation.navigate('SearchAddress')}}>
                    <Text
                        style={this.props.address.street ? fakeInputText : fakeInputTextDull}
                    >
                        {this.props.address.street || 'Around current location'}
                    </Text>
                </Text>,
                <View
                    key={'smallButtonContainer'}
                    style={smallButtonContainer}
                >
                    <SmallButton
                        key={'all'}
                        onPress={() => {
                            this.selectSport('all')
                        }}
                        active={this.props.sport === 'all'}
                    >
                        {'All'}
                    </SmallButton>

                    { this.renderDefaultSportsOrSelected() }

                    <SmallButton
                        key={'other'}
                        onPress={() => {this.props.navigation.navigate('SearchSport')}}
                    >
                        {'Other...'}
                    </SmallButton>
                </View>,
                <Button
                    key={'searchButton'}
                    onPress={this.validateLocation.bind(this)}>
                    Search activity
                </Button>
            ])
        } else {
            return null;
        }
    }

    renderCreateActivityIcon(){
        if (this.props.token){
            return (
                <Icon
                    size={25}
                    name='create'
                    color={styles.iconStyle.color}
                    iconStyle={styles.iconStyle}
                    containerStyle={styles.iconContainerStyle}
                    onPress={() => {this.props.navigation.navigate('CreateActivity')}}
                    raised={true}
                />
            )
        } else {
            return null;
        }
    }

    renderMenuIcon() {
        const {
            menuIconStyle
        } = styles;

        if (this.props.token) {
            return (
                <Icon
                    underlayColor={Colors[Colors.currentTheme].button.primary.bg}
                    name='menu'
                    color={Colors[Colors.currentTheme].button.primary.text}
                    containerStyle={menuIconStyle}
                    size={40}
                    onPress={() => this.props.navigation.navigate('DrawerOpen')}
                />
            )
        } else {
            return null;
        }
    }

    renderDefaultSportsOrSelected() {
        let currentSport = this.props.sport;
        if (!this.state.extendSportList) {
            return ([
                <SmallButton
                    key={'running'}
                    onPress={() => {this.selectSport('running')}}
                    active={currentSport === 'running'}>
                    {'Running'}
                </SmallButton>,
                <SmallButton
                    key={'football'}
                    onPress={() => {this.selectSport('football')}}
                    active={currentSport === 'football'}>
                    {'Football'}
                </SmallButton>
                ]);
        } else {
            return (
                <SmallButton
                    key={'other2'}
                    onPress={() => {this.selectSport(this.state.extraSport)}}
                    active={currentSport === this.state.extraSport}>
                    {toTitleCase(this.state.extraSport)}
                </SmallButton>
            )
        }
    }

    validateLocation() {
        if (this.props.address.street === null) {
            this.props.getCurrentLocation()
            .then(() => {
                this.gotoSearchActivities();
            })
            .catch(() => {
                // we should tell the user to type an address or activate the GPS
                // but maybe we do that already in the action creator
            })
        } else {
            this.gotoSearchActivities();
        }
    }

    selectSport(sport) {
        if (!['running', 'football', 'all'].includes(sport)) {
            this.setState({extendSportList: true, extraSport: sport});
        }
        this.props.sportChanged(sport);
    }

    gotoSearchActivities() {
        let { navigate } = this.props.navigation;
        navigate('Activities');
    }
}

Home.defaultProps = {
    sport: 'all',
    token: 'Missing token'
};

const styles = {
    background: {
        flex: 1,
        height: 200,
        width: undefined,
    },
    title: {
        fontSize: 14,
        marginHorizontal: 10,
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'left',
        borderBottomWidth: 0
    },
    defaultText: {
        // TODO: use font family inherit to avoid repetition
        marginHorizontal: 10,
        marginTop: 5,
        marginBottom: 2,
    },
    fakeInput: {
        borderWidth: 0,
        borderBottomWidth: 0.5,
        borderColor: Colors[Colors.currentTheme].input.onDark.border,
        color: Colors[Colors.currentTheme].input.onDark.text,
        fontSize: 18,
        marginHorizontal: 30,
        marginVertical: 0,
        paddingHorizontal: 10,
        paddingVertical: 10,
        backgroundColor: 'transparent',
        // NOTE: shadow works only on IOS
        // use elevation for android
        elevation: 0,
    },
    fakeInputText: {
        color: Colors[Colors.currentTheme].input.onDark.text,
    },
    fakeInputTextDull: {
        color: Colors[Colors.currentTheme].input.placeholder,
    },
    smallButtonContainer: {
        paddingHorizontal: 20,
        paddingTop: 0,
        paddingBottom: 20,
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row',
        marginVertical: 10
    },
    iconStyle: {
        color: Colors[Colors.currentTheme].button.primary.text
    },
    iconContainerStyle: {
        backgroundColor: Colors[Colors.currentTheme].button.primary.bg,
        position: 'absolute',
        bottom: 20,
        right: 20,
        zIndex: 100
    },
    menuIconStyle: {
        position: 'absolute',
        zIndex: 2,
        top: 5,
        left: 5,
        margin: 0,
        marginRight: 2,
        paddingLeft: 10,
        paddingRight: 15,
        paddingTop: 7,
        paddingBottom: 5,
    }
};

const mapStateToProps = (state) => {
    const { auth, search, notifications } = state;
    const {
        token,
    } = auth;

    const {
        message,
        loading
    } = notifications;

    const {
        address,
        sport
    } = search;

    return {
        loading,
        token,
        message,
        address,
        sport
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        tokenGet: () => {
            dispatch(tokenGet());
        },
        addressChanged: (address) => {
            dispatch(addressChanged(address));
        },
        sportChanged: (sport) => {
            dispatch(sportChanged(sport));
        },
        getCurrentLocation: () => {
            return dispatch(getCurrentLocation());
        },
        clearNofication: (enable, delay) => {
            if (enable) {
                setTimeout(function () {
                    dispatch(clearNotification());
                }, delay || 6000);
            }
        },
        isOffline: () => {
            dispatch(isOffline());
            dispatch(showOfflineMessage());
        }
    }
};


// wire Home to Store and wire the actions
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
import React from 'react';
import { DrawerItems, SafeAreaView } from 'react-navigation';
import { StyleSheet, ScrollView } from 'react-native';

const JWDrawer = (props) => (
    <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
        <DrawerItems {...props} items={
            props.items.filter((item) =>
                item.routeName === 'Home' ||
                item.routeName === 'Activities' ||
                item.routeName === 'MyActivities' ||
                item.routeName === 'About')
            }
        />
        </SafeAreaView>
    </ScrollView>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

export default JWDrawer;

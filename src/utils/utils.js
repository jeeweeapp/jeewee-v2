import { Linking } from 'react-native';

export const networkErrorMessage = 'Whoops...\n\n' +
    'Jeewee could not connect to internet.\n' +
    'Please check your connection and restart the app.';

export const gpsFailMessage = 'Whoops!\n\n' +
    'Jeewee could not determine your current location.\n' +
    'Please enable the GPS for a better addresses resolution.';

export const filterByPropNameWithRegex = (data, needle, property) => {
    let re = new RegExp(needle,"i");
    return data.filter(function(item){
        if (property) {
            // we work with array of objects
            return re.test(item[property]);
        } else {
            // we works with array of strings
            return re.test(item);
        }
    });
}

export const toTitleCase = (str) => {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

export const toCapFirst = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}


/**
 * See http://stackoverflow.com/a/21623206/2439580
 * @param pointA
 * @param pointB
 * @returns {number}
 */
export const getLinearDistanceBetween = (pointA, pointB) => {
    let lat1 = pointA.n;
    let lat2 = pointB.n;
    let lon1 = pointA.e;
    let lon2 = pointB.e;

    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((lat2 - lat1) * p)/2 +
        c(lat1 * p) * c(lat2 * p) *
        (1 - c((lon2 - lon1) * p))/2;

    let result = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    return Math.round(result * 100) / 100;
}

export const getTimeoutPromise = (milliseconds = 10000, message = 'Request timed out.\nPlease check your internet connection and retry.') => {
    return new Promise((resolve, reject) => {
        setTimeout(reject, milliseconds, message);
    });
}

export const isInThePast = (dateString) => {
    var now = Date.now();
    var date = new Date(dateString);
    return date < now;
}

export const warn = (warningObj) => {
    if (__DEV__) {
        console.warn(JSON.stringify(warningObj));
    }
}

export const openLink = (url) => {
    Linking.canOpenURL(url).then(supported => {
        if (!supported) {
            console.warn('Can\'t handle url: ' + url);
        } else {
            return Linking.openURL(url);
        }
    }).catch(err => console.error('An error occurred', err));
}
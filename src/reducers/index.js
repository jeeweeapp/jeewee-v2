import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import LocationReducer from './LocationReducer';
import SearchActivityReducer from './SearchActivityReducer';
import ActivitiesReducer from './ActivitiesReducer';
import NotificationReducer from './NotificationReducer';
import NavigationReducer from './NavigationReducer';
import VenuesReducer from './VenuesReducer';
import ConnectivityReducer from './ConnectivityReducer';

export default combineReducers({
    auth: AuthReducer,
    search: SearchActivityReducer,
    activities: ActivitiesReducer,
    notifications: NotificationReducer,
    navigation: NavigationReducer,
    venues: VenuesReducer,
    location: LocationReducer,
    connectivity: ConnectivityReducer
});
